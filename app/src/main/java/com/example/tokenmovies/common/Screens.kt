package com.example.tokenmovies.common

import androidx.fragment.app.Fragment
import com.example.tokenmovies.scenarios.movie_detail.MovieDetailFragment
import com.example.tokenmovies.scenarios.movies_list.MoviesListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class MoviesListScreen: SupportAppScreen(){
        override fun getFragment(): Fragment {
            return MoviesListFragment()
        }
    }

    class MovieDetailScreen(val id: String) : SupportAppScreen(){
        override fun getFragment(): Fragment {
            return MovieDetailFragment.newInstance(id)
        }
    }
}