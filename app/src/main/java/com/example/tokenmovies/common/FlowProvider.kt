package com.example.tokenmovies.common

import ru.terrakok.cicerone.Router

interface FlowProvider {
    fun provideRouter(): Router
}