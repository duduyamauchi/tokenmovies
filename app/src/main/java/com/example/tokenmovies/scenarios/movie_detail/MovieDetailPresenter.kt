package com.example.tokenmovies.scenarios.movie_detail

import android.content.Context
import com.example.tokenmovies.entities.MovieDetail
import com.example.tokenmovies.repository.MoviesRepository

class MovieDetailPresenter (val movieId: String, val view: MovieDetailContract.View, val context: Context): MovieDetailContract.Presenter {
    override fun loadDetails() {
        view.showLoading()

        val repository = MoviesRepository(context)

        val onSuccess: ((movie: MovieDetail, isFromCache: Boolean) -> Unit) = {movie, isFromCache ->
            view.hideLoading()

            if (isFromCache) view.showToast("Detalhe carregado do cache")
            view.showDetails(movie)
        }

        val onFailure: ((t:Throwable) -> Unit) = {
            view.hideLoading()
            view.showError()
        }

        repository.getMovieByID(movieId,onSuccess,onFailure)
    }
}