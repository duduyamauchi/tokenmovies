package com.example.tokenmovies.scenarios.movies_list

import android.content.Context
import com.example.tokenmovies.entities.MoviesListItem
import com.example.tokenmovies.repository.MoviesRepository

class MoviesListPresenter(val view: MoviesListContract.View, val context: Context) : MoviesListContract.Presenter {
    override fun onLoadList(){

        view.showLoading()

        val repository = MoviesRepository(context)

        val onSuccess: ((movies: List<MoviesListItem>, isFromCache: Boolean) -> Unit) = {movies, isFromCache ->
            view.hideLoading()
            if (isFromCache) view.showToast("Filmes carregados do cache")
            view.showList(movies)
        }

        val onFailure: ((t: Throwable) -> Unit) = {
            view.hideLoading()
            view.showError()
        }

        repository.getMoviesList(onSuccess,onFailure)
    }


}