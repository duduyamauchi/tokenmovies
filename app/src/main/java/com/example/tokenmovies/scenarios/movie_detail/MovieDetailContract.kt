package com.example.tokenmovies.scenarios.movie_detail

import com.example.tokenmovies.entities.MovieDetail

interface MovieDetailContract {

    interface View{
        fun showDetails(movie: MovieDetail)
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun showToast(msg: String)
    }

    interface Presenter{
        fun loadDetails()
    }
}