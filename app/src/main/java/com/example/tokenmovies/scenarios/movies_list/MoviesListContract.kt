package com.example.tokenmovies.scenarios.movies_list

import com.example.tokenmovies.entities.MoviesListItem

interface MoviesListContract {

    interface Presenter{
        fun onLoadList()
    }

    interface View{
        fun showToast(msg: String)
        fun showError()
        fun showList(movies: List<MoviesListItem>)
        fun showLoading()
        fun hideLoading()
    }
}