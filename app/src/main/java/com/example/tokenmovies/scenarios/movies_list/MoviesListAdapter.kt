package com.example.tokenmovies.scenarios.movies_list

import android.content.Context
import android.widget.AdapterView
import com.bumptech.glide.Glide
import com.example.tokenmovies.R
import com.example.tokenmovies.entities.MoviesListItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.movies_list_item.view.*
import java.lang.StringBuilder

class MoviesListAdapter (private val context: Context,
                         movies: List<MoviesListItem>,
                         val itemClickListener: ((moviesListItem: MoviesListItem) -> Unit)) : GroupAdapter<GroupieViewHolder>() {

    init {
        movies.forEach {movie ->
            add(MoviesItem(movie))

        }
    }

    inner class MoviesItem(val movie: MoviesListItem) : Item<GroupieViewHolder>(){
        override fun getLayout(): Int = R.layout.movies_list_item


        override fun bind(viewHolder: GroupieViewHolder, position: Int) {


            val builder = StringBuilder()

            movie.genres.forEach {
                builder.append(it)
                builder.append(", ")
            }

            builder.deleteCharAt(builder.length - 2) // para apagar a virgula final

            viewHolder.itemView.tvMovieListItemTitle.text = movie.title
            viewHolder.itemView.tvMovieListItemGenre.text = builder.toString()
            viewHolder.itemView.tvMovieAverage.text = movie.vote_average

            Glide.with(context)
                .load(movie.poster_url)
                .placeholder(R.drawable.ic_local_movies)
                .circleCrop()
                .into(viewHolder.itemView.imgMoviePoster)

            viewHolder.itemView.setOnClickListener{
                itemClickListener(movie)
            }
        }



    }
}