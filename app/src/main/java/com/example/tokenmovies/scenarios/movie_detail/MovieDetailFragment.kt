package com.example.tokenmovies.scenarios.movie_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.tokenmovies.R
import com.example.tokenmovies.entities.MovieDetail
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class MovieDetailFragment: Fragment(), MovieDetailContract.View {

    companion object{

        fun newInstance(id: String): MovieDetailFragment{
            val fragment = MovieDetailFragment()
            fragment.arguments = Bundle().apply {
                putString("id",id)
            }
            return fragment
        }
    }
    override fun showDetails(movie: MovieDetail) {
        Glide.with(this)
            .load(movie.poster_url)
            .fitCenter()
            .into(this.imgMovieDetail)

        if (movie.tagline.equals("")){
            this.tvTagline.visibility = TextView.GONE
        } else {
            val tagline = "\"${movie.tagline}\""
            this.tvTagline.text = tagline
        }

        val builder = StringBuilder()

        movie.genres.forEach {
            builder.append(it)
            builder.append(", ")
        }

        builder.deleteCharAt(builder.length - 2) // para apagar a virgula final



        this.tvMovieTitle.text = movie.title
        this.tvOverview.text = movie.overview
        this.tvGenre.text = builder.toString()
        this.tvReleaseDate.text = movie.release_date
        this.tvVoteAverage.text = movie.vote_average

    }

    override fun showLoading() {
        detailsPlaceholder.visibility = FrameLayout.INVISIBLE
        pbLoadingDetails?.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        pbLoadingDetails?.visibility = ProgressBar.INVISIBLE
        detailsPlaceholder.visibility = FrameLayout.VISIBLE
    }

    override fun showError() {
        val drinkId = getDrinkId()
        detailsPlaceholder.visibility = FrameLayout.INVISIBLE
        fmErrorDetails.visibility = FrameLayout.VISIBLE

        errorDetailButton.setOnClickListener{
            fmErrorDetails.visibility = FrameLayout.INVISIBLE
            activity?.let {activity ->
                val presenter: MovieDetailContract.Presenter = MovieDetailPresenter(drinkId!!, this, activity)
                presenter.loadDetails()
            }
        }
    }

    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    private fun getDrinkId() = arguments?.getString("id")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val drinkId = getDrinkId()

        activity?.let {activity ->
            val presenter: MovieDetailContract.Presenter = MovieDetailPresenter(drinkId!!, this, activity)
            presenter.loadDetails()
        }
    }
}