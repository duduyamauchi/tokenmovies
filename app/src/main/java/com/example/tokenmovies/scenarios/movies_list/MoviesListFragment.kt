package com.example.tokenmovies.scenarios.movies_list

import android.icu.lang.UCharacter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tokenmovies.R
import com.example.tokenmovies.common.FlowProvider
import com.example.tokenmovies.common.Screens
import com.example.tokenmovies.entities.MoviesListItem
import kotlinx.android.synthetic.main.fragment_movies_list.*

class MoviesListFragment: Fragment(), MoviesListContract.View {

    val router by lazy {
        (this.activity as FlowProvider).provideRouter()
    }


    override fun showToast(msg: String) {
        Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        listPlaceholder.visibility = FrameLayout.INVISIBLE
        fmErrorList.visibility = FrameLayout.VISIBLE

        errorListButton?.setOnClickListener{
            fmErrorList.visibility =  FrameLayout.INVISIBLE
            activity?.let {activity ->
                val presenter : MoviesListContract.Presenter = MoviesListPresenter(this,activity)
                presenter.onLoadList()
            }
        }
    }

    override fun showList(movies: List<MoviesListItem>) {

        activity?.let {activity ->
            val adapter = MoviesListAdapter(activity,movies){
                router.navigateTo(Screens.MovieDetailScreen(it.id))
            }

            val decoration = DividerItemDecoration(activity,1)

            rvMovies.addItemDecoration(decoration)
            rvMovies.adapter = adapter
            rvMovies.layoutManager = LinearLayoutManager(activity)
        }
    }

    override fun showLoading() {
        listPlaceholder.visibility = FrameLayout.INVISIBLE
        pbLoadingList?.visibility = ProgressBar.VISIBLE
    }

    override fun hideLoading() {
        pbLoadingList?.visibility = ProgressBar.INVISIBLE
        listPlaceholder.visibility = FrameLayout.VISIBLE
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {activity ->
            val presenter : MoviesListContract.Presenter = MoviesListPresenter(this,activity)
            presenter.onLoadList()
        }
    }
}