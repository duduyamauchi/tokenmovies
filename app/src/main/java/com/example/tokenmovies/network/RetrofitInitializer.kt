package com.example.tokenmovies.network

import com.example.tokenmovies.network.MoviesProvider.Companion.ENDPOINT
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    val retrofit = Retrofit.Builder()
        .baseUrl(ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun createMoviesProvider() = retrofit.create(MoviesProvider::class.java)

}