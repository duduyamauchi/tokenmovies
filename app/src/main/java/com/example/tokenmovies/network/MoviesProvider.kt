package com.example.tokenmovies.network

import com.example.tokenmovies.entities.MovieDetail
import com.example.tokenmovies.entities.MoviesListItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesProvider {

    companion object{
        public const val ENDPOINT = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"
    }

    @GET("movies")
    fun getMoviesList(): Call<List<MoviesListItem>>

    @GET("movies/{id}")
    fun getMovieByID(@Path("id") id: String) : Call<MovieDetail>
}