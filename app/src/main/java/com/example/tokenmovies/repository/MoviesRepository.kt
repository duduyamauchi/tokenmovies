package com.example.tokenmovies.repository

import android.content.Context
import android.graphics.Movie
import com.example.tokenmovies.entities.MovieDetail
import com.example.tokenmovies.entities.MoviesListItem
import com.example.tokenmovies.network.RetrofitInitializer
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class MoviesRepository(private val context: Context) {

    companion object{
        val moviesProvider = RetrofitInitializer().createMoviesProvider()
    }

    fun getMoviesList(onSuccess: ((movies: List<MoviesListItem>, isFromCache: Boolean) -> Unit),
                      onFailure: ((t: Throwable) -> Unit)){

        val paperKey = "MoviesList"

        var movies: List<MoviesListItem>?

        val callback : Callback<List<MoviesListItem>> =
            (object : Callback<List<MoviesListItem>> {
                override fun onFailure(call: Call<List<MoviesListItem>>, t: Throwable) {
                    movies = Paper.book().read(paperKey)

                    if (movies != null){
                        onSuccess(movies!!, true)
                    } else {
                        onFailure(t)
                    }
                }

                override fun onResponse(call: Call<List<MoviesListItem>>, response: Response<List<MoviesListItem>>) {
                    movies = response.body()

                    if (movies != null){
                        Paper.book().write(paperKey, movies)
                        onSuccess(movies!!,false)
                    } else {
                        movies = Paper.book().read(paperKey)
                        if (movies != null){
                            onSuccess(movies!!, true)
                        } else {
                            onFailure(Exception("Não há dados para serem mostrados"))
                        }
                    }
                }
            })
        moviesProvider.getMoviesList().enqueue(callback)
    }

    fun getMovieByID(id: String,
                     onSuccess: ((movie: MovieDetail, isFromCache: Boolean) -> Unit),
                     onFailure: ((t: Throwable) -> Unit)){

        val paperKey = id

        var movie: MovieDetail?

        val callback: Callback<MovieDetail> =
            (object : Callback<MovieDetail>{
                override fun onFailure(call: Call<MovieDetail>, t: Throwable) {
                    movie = Paper.book().read(paperKey)
                    if (movie != null){
                        onSuccess(movie!!,true)
                    } else {
                        onFailure(t)
                    }
                }

                override fun onResponse(call: Call<MovieDetail>, response: Response<MovieDetail>) {
                    movie = response.body()
                    if (movie != null){
                        Paper.book().write(paperKey,movie)
                        onSuccess(movie!!,false)
                    } else{
                        movie = Paper.book().read(paperKey)
                        if (movie != null){
                            onSuccess(movie!!, true)
                        } else {
                            onFailure(Exception("Não há detalhes para serem carregados"))
                        }
                    }
                }
            })
        moviesProvider.getMovieByID(id).enqueue(callback)

    }

}