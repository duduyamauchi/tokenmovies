package com.example.tokenmovies.entities

data class MovieDetail (val title: String,
                        val tagline: String,
                        val release_date: String,
                        val overview: String,
                        val genres: List<String>,
                        val vote_average: String,
                        val poster_url: String)