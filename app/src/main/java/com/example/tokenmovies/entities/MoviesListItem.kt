package com.example.tokenmovies.entities

data class MoviesListItem (val id: String,
                           val vote_average: String,
                           val title: String,
                           val poster_url: String,
                           val genres: List<String>)
